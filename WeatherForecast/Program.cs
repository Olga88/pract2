﻿using System;
using System.Buffers.Text;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using static WeatherForecast.WeatherModels;

namespace WeatherForecast
{
    public class DateTimeConverterUsingDateTimeParse : JsonConverter<DateTime>
    {
        public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(reader.GetInt32()).ToLocalTime();
            return dtDateTime;
        }

        public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan diff = value.ToUniversalTime() - origin;
            double result = Math.Floor(diff.TotalSeconds);
            writer.WriteNumberValue(result);
        }
    }
    class Program
    {
        static readonly HttpClient client = new HttpClient();

        static async Task Main(string[] args)
        {                        
            WeatherForecast forecast = new WeatherForecast();
            CurrentWeather currentWeather = await forecast.GetCurrentWeatherAsync(60.99, 30.9);
            DailyForecast dailyForecast = await forecast.GetDailyForecastAsync(60.99, 30.9, "hourly,daily");
            DailyHistoricalWeather dailyHistoricalWeather = await forecast.GetDailyHistoricalWeatherAsync(60.99, 30.9, new DateTime(2021,05,29));

            //CurrentWeather
            Console.WriteLine("Current weather");
            Console.WriteLine(currentWeather.name);
            Console.WriteLine(currentWeather.dt);
            foreach (Weather w in currentWeather.weather)
            {
                Console.WriteLine(w.description);
            };
            Console.WriteLine(currentWeather.main.temp);

            //DailyForecast
            Console.WriteLine("DailyForecast");
            Console.WriteLine(dailyForecast.current.dt);
            Console.WriteLine(dailyForecast.current.sunrise);

            //DailyHistoricalWeather
            Console.WriteLine("DailyHistoricalWeather");
            Console.WriteLine(dailyHistoricalWeather.current.dt);
            Console.WriteLine(dailyHistoricalWeather.current.pressure);            
        }

        public class WeatherForecast
        {
            JsonSerializerOptions options = new JsonSerializerOptions();
            
            public WeatherForecast()
            {
                
            }
           
            public async Task<CurrentWeather> GetCurrentWeatherAsync(double lat, double lon)
            {
                options.Converters.Add(new DateTimeConverterUsingDateTimeParse());
                string URI = string.Format($"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid=ca3eed7bbe688c4e97e57937ecdcb4c5", lat, lon);
                string data = await client.GetStringAsync(URI);
                CurrentWeather weatherForecast = JsonSerializer.Deserialize<CurrentWeather>(data, options);
                return weatherForecast;
            }
            public async Task<DailyForecast> GetDailyForecastAsync(double lat, double lon, string exclude)
            {
                //options.Converters.Add(new DateTimeConverterUsingDateTimeParse());
                string URI = string.Format($"https://api.openweathermap.org/data/2.5/onecall?lat={lat}&lon={lon}&exclude={exclude}&appid=ca3eed7bbe688c4e97e57937ecdcb4c5", lat, lon, exclude);
                string data = await client.GetStringAsync(URI);
                DailyForecast daily = JsonSerializer.Deserialize<DailyForecast>(data, options);
                return daily;
            }
            public async Task<DailyHistoricalWeather> GetDailyHistoricalWeatherAsync(double lat, double lon, DateTime dt)
            {
                //options.Converters.Add(new DateTimeConverterUsingDateTimeParse());
                string r = JsonSerializer.Serialize(dt, options);
                string URI = string.Format($"https://api.openweathermap.org/data/2.5/onecall/timemachine?lat={lat}&lon={lon}&dt={r}&appid=ca3eed7bbe688c4e97e57937ecdcb4c5", lat, lon, r);
                string data = await client.GetStringAsync(URI);
                DailyHistoricalWeather dailyHistorical = JsonSerializer.Deserialize<DailyHistoricalWeather>(data, options);
                return dailyHistorical;
            }
        }
    }
}
